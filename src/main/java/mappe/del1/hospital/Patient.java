package mappe.del1.hospital;

/**
 * The type Patient.
 */
public class Patient extends Person implements Diagnosable {

    private String diagnosis = "";

    /**
     * Instantiates a new Patient.
     *
     * @param firstName            the first name
     * @param lastName             the last name
     * @param socialSecurityNumber the social security number
     */
    protected Patient(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * Gets diagnosis.
     *
     * @return the diagnosis
     */
    protected String getDiagnosis() {
        return diagnosis;
    }

    /**
     * Sets diagnosis.
     *
     * @param diagnosis the diagnosis
     */
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    @Override
    public String toString() {
        return "firstName = " + getFirstName() +
                System.lineSeparator() +
                "lastName = " + getLastName() +
                System.lineSeparator() +
                "socialSecurityNumber = " + getSocialSecurityNumber() +
                System.lineSeparator() +
                "diagnosis = " + this.diagnosis;
    }
}
