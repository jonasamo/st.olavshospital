package mappe.del1.hospital;

import mappe.del1.hospital.healthpersonal.Nurse;
import mappe.del1.hospital.healthpersonal.doctor.GeneralPractitioner;
import mappe.del1.hospital.healthpersonal.doctor.Surgeon;

/**
 * The purpose of this class is solely to fill the program with data about patients, employees, departments etc.
 */
public final class HospitalTestData {

    /**
     * Instantiates a new Hospital test data.
     */
    public HospitalTestData() {
        // Not called
    }

    /**
     * Fill register with test data.
     *
     * @param hospital the hospital
     */
    public static void fillRegisterWithTestData (final Hospital hospital) {

        // Add some departments
        Department emergency = new Department("Akutten");
        emergency.getEmployees().add(new Employee("OddEven","Primtallet","1"));
        emergency.getEmployees().add(new Employee("Huppasahn","DelFinito","2"));
        emergency.getEmployees().add(new Employee("Rigmor","Mortis","3"));
        emergency.getEmployees().add(new GeneralPractitioner("Inco","Gnito","4"));
        emergency.getEmployees().add(new Surgeon("Inco","Gnito","5"));
        emergency.getEmployees().add(new Nurse("Nina","Teknologi","6"));
        emergency.getEmployees().add(new Nurse("Ove","Ralt","7"));
        emergency.getPatients().add(new Patient("Inga","Lykke","8"));
        emergency.getPatients().add(new Patient("Ulrik","Smål","9"));
        hospital.getDepartments().add(emergency);

        Department childrenPolyclinic = new Department("Barnpoliklinikk");
        childrenPolyclinic.getEmployees().add(new Employee("Salti","Kaffen","10"));
        childrenPolyclinic.getEmployees().add(new Employee("NidelV.","Elvefølger","11"));
        childrenPolyclinic.getEmployees().add(new Employee("Anton","Nym","12"));
        childrenPolyclinic.getEmployees().add(new GeneralPractitioner("Gene","Sis","13"));
        childrenPolyclinic.getEmployees().add(new Surgeon("Nanna","Na","14"));
        childrenPolyclinic.getEmployees().add(new Nurse("Nora","Toriet","15"));
        childrenPolyclinic.getPatients().add(new Patient("Hans","Omvar","16"));
        childrenPolyclinic.getPatients().add(new Patient("Laila","La","17"));
        childrenPolyclinic.getPatients().add(new Patient("Jøran","Drebli","18"));
        hospital.getDepartments().add(childrenPolyclinic);
    }
}
