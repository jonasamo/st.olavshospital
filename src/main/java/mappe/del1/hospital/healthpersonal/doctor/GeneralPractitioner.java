package mappe.del1.hospital.healthpersonal.doctor;

import mappe.del1.hospital.Patient;

/**
 * The type General practitioner.
 */
public class GeneralPractitioner extends Doctor {

    /**
     * Instantiates a new General practitioner.
     *
     * @param firstname            the firstname
     * @param lastName             the last name
     * @param socialSecurityNumber the social security number
     */
    public GeneralPractitioner(String firstname, String lastName, String socialSecurityNumber) {
        super(firstname, lastName, socialSecurityNumber);
    }

    @Override
    public void setDiagnosis(Patient patient, String diagnosis) {
        patient.setDiagnosis(diagnosis);
    }
}
