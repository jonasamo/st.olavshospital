package mappe.del1.hospital.healthpersonal;

import mappe.del1.hospital.Employee;

/**
 * The type Nurse.
 */
public class Nurse extends Employee {

    /**
     * Instantiates a new Nurse.
     *
     * @param firstName            the first name
     * @param lastName             the last name
     * @param socialSecurityNumber the social security number
     */
    public Nurse(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public String toString() {
        return "firstName = " + getFirstName() +
                System.lineSeparator() +
                "lastName = " + getLastName() +
                System.lineSeparator() +
                "socialSecurityNumber = " + getSocialSecurityNumber();
    }
}
