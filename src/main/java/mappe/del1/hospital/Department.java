package mappe.del1.hospital;

import mappe.del1.hospital.exception.RemoveException;

import java.util.List;
import java.util.ArrayList;
import java.util.Objects;

/**
 * Represents a department in a hospital.
 * A department has a name, a list of patients and a list of employees.
 */
public class Department {

    private String departmentName;
    private final List<Employee> employees;
    private final List<Patient> patients;

    /**
     * Instantiates a new Department.
     *
     * @param departmentName the department name
     */
    public Department(String departmentName) {
        this.departmentName = departmentName;
        this.employees = new ArrayList<>();
        this.patients = new ArrayList<>();
    }

    /**
     * Sets department name.
     *
     * @param departmentName the department name
     */
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    /**
     * Gets department name.
     *
     * @return the department name
     */
    public String getDepartmentName() {
        return departmentName;
    }

    /**
     * Gets employees.
     *
     * @return the employees
     */
    public List<Employee> getEmployees() {
        return employees;
    }

    /**
     * Add employee.
     *
     * @param employee the employee
     */
    public void addEmployee(Employee employee) {
        employees.add(employee);
    }

    /**
     * Gets patients.
     *
     * @return the patients
     */
    public List<Patient> getPatients() {
        return patients;
    }

    /**
     * Add patient.
     *
     * @param patient the patient
     */
    public void addPatient(Patient patient) {
        patients.add(patient);
    }

    /**
     * This method removes a Person-object from the department.
     * It iterates through the list patient or the list employees based on the type of the Person-object.
     * If no match was found in neither patients nor employees, an RemoveException is thrown.
     * If a null-object is given as argument, an RemoveException is thrown.
     *
     * @param person the person
     * @throws RemoveException the remove exception
     */
    public void remove(Person person) throws RemoveException {

        if (person == null) {
            throw new RemoveException("Person is a null-object.");
        }

        Person foundPerson = null;

        if (person instanceof Patient) {
            for (Person patient : patients) {
                if (patient.getSocialSecurityNumber().equals(person.getSocialSecurityNumber())) {
                    foundPerson = patient;
                }
            }
        } else if (person instanceof Employee) {
            for (Person employee : employees) {
                if (employee.getSocialSecurityNumber().equals(person.getSocialSecurityNumber())) {
                    foundPerson = employee;
                }
            }
        }

        if (foundPerson == null) {
            throw new RemoveException("Person does not exist in either list employees or list patients.");
        } else if (foundPerson instanceof Employee) {
            employees.remove(foundPerson);
        } else {
            patients.remove(foundPerson);
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDepartmentName(), getEmployees(), getPatients());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Department)) return false;
        Department that = (Department) o;
        return Objects.equals(getDepartmentName(), that.getDepartmentName()) && Objects.equals(getEmployees(), that.getEmployees()) && Objects.equals(getPatients(), that.getPatients());
    }

    @Override
    public String toString() {
        return "departmentName = " + departmentName +
                System.lineSeparator() +
                "employees = " + employees +
                System.lineSeparator() +
                "patients = " + patients;
    }
}

