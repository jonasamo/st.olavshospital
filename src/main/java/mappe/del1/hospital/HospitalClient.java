package mappe.del1.hospital;

import mappe.del1.hospital.exception.RemoveException;

/**
 * A simple client to check whether the remove()-method in the Department-class is working or not.
 */
public class HospitalClient {

    /**
     * The entry point of application.
     *
     * @throws RemoveException the remove exception
     */
    public static void main(String[] args) throws RemoveException {
        Hospital hospital = new Hospital("St. Olavs hospital");
        HospitalTestData.fillRegisterWithTestData(hospital);

        // Prints out all employees in the emergency department before the removal.
        for (Person person : hospital.getDepartments().get(0).getEmployees()) {
            System.out.println(person);
            System.out.println();
        }

        System.out.println("####################################################################");

        Employee employee = new Employee("OddEven", "Primtallet", "1");
        hospital.getDepartments().get(0).remove(employee);

        // Prints out all employees in the emergency department after the removal.
        // Employee with socialSecurityNumber = 1 has now been removed.
        for (Person person : hospital.getDepartments().get(0).getEmployees()) {
            System.out.println(person);
            System.out.println();
        }

        // Tries to remove a person that does not exist in either list patients or list employees in the emergency department.
        try {
            Patient patient = new Patient("OddEven", "Primtallet", "99");
            hospital.getDepartments().get(0).remove(patient);
        } catch (RemoveException e) {
            e.printStackTrace();
        }
    }
}
