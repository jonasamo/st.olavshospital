package mappe.del1.hospital;

import java.util.List;
import java.util.ArrayList;

/**
 * Represents a hospital in the hospital system.
 * A hospital has a name and a list of departments.
 */
public class Hospital {

    private final String hospitalName;
    private final List<Department> departments;

    /**
     * Instantiates a new Hospital.
     *
     * @param hospitalName the hospital name
     */
    public Hospital(String hospitalName) {
        this.hospitalName = hospitalName;
        this.departments = new ArrayList<>();
    }

    /**
     * Gets hospital name.
     *
     * @return the hospital name
     */
    public String getHospitalName() {
        return hospitalName;
    }

    /**
     * Gets departments.
     *
     * @return the departments
     */
    public List<Department> getDepartments() {
        return departments;
    }

    /**
     * Add department.
     *
     * @param department the department
     */
    public void addDepartment(Department department) {
        departments.add(department);
    }

    @Override
    public String toString() {
        return "hospitalName = " + hospitalName +
                System.lineSeparator() +
                "departments = " + departments;
    }
}
