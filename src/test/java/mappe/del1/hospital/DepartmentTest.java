package mappe.del1.hospital;

import mappe.del1.hospital.exception.RemoveException;
import mappe.del1.hospital.healthpersonal.Nurse;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Should perform the following tests:
 *
 * Positive tests:
 * - Test that remove() removes Patient-object from list patients.
 * - Test that remove() removes Employee-object from list employees.
 * - Test that remove() gives feedback when trying to remove a non-existing Person-object.
 *
 * Negative tests:
 * - Test remove() with invalid input, for instance a null-object.
 */
public class DepartmentTest {

    /**
     * - Tests that remove() removes a existing Patient-object from list patients.
     *
     * @throws RemoveException
     */
    @Test
    @DisplayName("Tests that a existing patient gets removed from list patients")
    public void testThatAPatientGetsRemovedUsingValidInput() throws RemoveException {
        try {
            Department department = new Department("departmentNameTest");
            Patient testPatient = new Patient("firstNameTest", "lastNameTest", "socialSecurityNumberTest");
            department.addPatient(testPatient);
            department.remove(testPatient);
            assertEquals(0, department.getEmployees().size());
        } catch (RemoveException e) {
            e.printStackTrace();
        }
    }

    /**
     * - Tests that remove() removes a existing Employee-object from list employees.
     *
     * @throws RemoveException
     */
    @Test
    @DisplayName("Tests that an existing employee gets removed from list employee")
    public void testThatAnEmployeeGetsRemovedUsingValidInput() throws RemoveException {
        try {
            Department department = new Department("departmentNameTest");
            Nurse testNurse = new Nurse("firstNameTest", "lastNameTest", "socialSecurityNumberTest");
            department.addEmployee(testNurse);
            department.remove(testNurse);
            assertEquals(0, department.getEmployees().size());
        } catch (RemoveException e) {
            e.printStackTrace();
        }
    }

    /**
     * - Tests that an exception is thrown if trying to remove a Person-object that
     * does not exist in either list patients or list employees.
     *
     * @throws RemoveException
     */
    @Test
    @DisplayName("Tests that an exception is thrown when removing non-existing Person-object.")
    public void testThatAnExceptionIsThrownForNonExistingObjects() throws RemoveException {
        try {
            Department department = new Department("departmentNameTest");
            Nurse testNurse = new Nurse("firstNameTest", "lastNameTest", "socialSecurityNumberTest");
            department.remove(testNurse);
            assertEquals(0, department.getEmployees().size());
        } catch (RemoveException e) {
            e.printStackTrace();
        }
    }

    // NEGATIVE TESTS:

    /**
     * - Tests that an exception is thrown if a null-object is given as argument to remove().
     *
     * @throws RemoveException
     */
    @Test
    @DisplayName("Tests that an exception is thrown if a null-object is given as argument to remove().")
    public void testThatAnExceptionIsThrownForANullObject() throws RemoveException {
        try {
            Department department = new Department("departmentNameTest");
            department.remove(null);
        } catch (RemoveException e) {
            e.printStackTrace();
        }
    }
}
